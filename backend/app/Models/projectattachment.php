<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class projectattachment extends Model
{
    use HasFactory;

    protected $fillable = [ //PostAnfrage
        'userId',
        'file',
        'ticketId'
    ];

    /*
     *  $table->integer('userId');
            $table->longText('file');
            $table->integer('ticketId');
     */
}
