<?php

namespace App\Http\Controllers;

use App\Models\ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    public function showProjectColumns($pid) //columnId
    {
        //Serverseitig den User holen
        $user = auth()->user();
        //Schauen, ob der User zugang zum Projekt hat.
        $userAccess= DB::select('SELECT * FROM `usertoprojects` where userId = :uid AND projectId = :pid', ['uid'=>$user->id, 'pid'=>$pid]);
        //Falls er Zugang hat, SQL Anfrage ausführen und Tickets zurückgeben.
        if($userAccess){
            $tickets = DB::select('SELECT tickets.id, tickets.titel, tickets.userIdCreator,tickets.userIdEditor ,tickets.projectId ,tickets.columnId ,tickets.description, tickets.endTime, tickets.priority, users.image AS editorprofilbild, users.name AS editorname FROM `tickets` LEFT JOIN users ON tickets.userIdEditor = users.id where projectId = :pid', ['pid'=>$pid]);

            if($tickets){
                return response($tickets, 200);
             }
        }


        return response('', 204);
    }

    public function update($id, Request $request) { //JSON {}
        $ticket = ticket::findOrFail($id);
        $ticket->update($request->all());
        return response($ticket, 200);
    }

    public function updateColumn($id, Request $request) {
        $affected = DB::table('tickets')
            ->where('id', $id)
            ->update(['columnId' => $request['columnId']]);

        return response($affected, 200);
    }

    public function deleteById($id){
        //DB::table('tickets')->where('id', $id)->delete();
        $ticket = ticket::findOrFail($id);
        $ticket->delete();

        if($ticket){
            return response($ticket, 200);
        }

        return response('', 204);
    }


    public function store(Request $request){ //name , adminid, image, description
        $user = auth()->user();

        $ticket = ticket::create($request->all());

        return response($ticket, 204);
    }


}
