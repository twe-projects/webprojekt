<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register','\App\Http\Controllers\AuthController@register');

Route::post('/login', '\App\Http\Controllers\AuthController@login');

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/tilltest', '\App\Http\Controllers\testcontroller@index');
    Route::post('/logout', '\App\Http\Controllers\AuthController@logout');
    Route::get('/usertoproject', '\App\Http\Controllers\UserToProjectController@showUserProjects');
    Route::get('/project/{id}', '\App\Http\Controllers\ProjectController@show');
    Route::post('/storeproject/', '\App\Http\Controllers\ProjectController@store');
    Route::get('/projectlist', '\App\Http\Controllers\ProjectController@showAllUserProjects');
    Route::get('/projectquantity', '\App\Http\Controllers\ProjectController@getQuantityProject');
    Route::get('/projectcolumns/{id}', '\App\Http\Controllers\ProjectColumnsController@showProjectColumns');
    Route::post('/updateprojectcolumns/{id}', '\App\Http\Controllers\ProjectColumnsController@update');
    Route::get('/tickets/{id}', '\App\Http\Controllers\TicketController@showProjectColumns');
    Route::post('/storetickets/', '\App\Http\Controllers\TicketController@store');
    Route::post('/upateticket/{id}', '\App\Http\Controllers\TicketController@update');
    Route::post('/updateticketcolumn/{id}', '\App\Http\Controllers\TicketController@updateColumn');
    Route::get('/getprojectsuser/{id}', '\App\Http\Controllers\ProjectController@getAllUser');
    Route::get('/getProjectAdmin/{id}', '\App\Http\Controllers\ProjectController@getProjectAdmin');
    Route::post('/addusertoproject/', '\App\Http\Controllers\UserToProjectController@addUserToProject');
    //getAllUser
    //$id, Request $request
    //showProjectColumns
});


