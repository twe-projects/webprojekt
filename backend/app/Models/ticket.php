<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
    use HasFactory;
    protected $fillable = [ //PostAnfrage
        'titel',
        'userIdCreator',
        'userIdEditor',
        'columnId',
        'projectId',
        'description',
        'endTime',
        'priority'
    ];

    /* /attachment
     *
     *      $table->String('titel');
            $table->integer('userIdCreator');
            $table->integer('userIdEditor');
            $table->integer('columnId');
            $table->String('description');
            $table->dateTime('endTime');
            $table->integer('priority');
     *
     */
}
