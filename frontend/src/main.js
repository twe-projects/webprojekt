import Vue from 'vue'
import App from './App.vue'
import './assets/tailwind.css';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';

import {store} from './store/store.js';

Vue.config.productionTip = false

Vue.use(VueAxios, axios);


new Vue({
  store: store,
  router,
  render: h => h(App)
}).$mount('#app')
