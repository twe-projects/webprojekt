<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usertoproject extends Model
{
    use HasFactory;

    protected $fillable = [ //PostAnfrage
        'userId',
        'projectId',
        'usertype'
    ];
    /*
          Datenbank Migration:
            $table->integer('userId');
            $table->integer('projectId');
            $table->integer('usertype'); // 1 == user , 2 == moderator , 3 == admin....

     */
}
