<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function register(Request $request) {
        $data = $request->validate([
            'image' => 'string',
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|confirmed'
        ]);

        /* User erstellen */
        if($data['image']) {
            $user = User::create([
                'image' => $data['image'],
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);
        } else {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password'])
            ]);
        }

        /* Token erstellen */
        $token = $user->createToken('apptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response,201);
    }

    public function login(Request $request) {
        $data = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        /* Email checken */
        $user = User::where('email', $data['email'])->first();

        /* Logindaten checken */
        if(!$user || !Hash::check($data['password'], $user->password)) {
            return response([
                'message' => 'Wrong password or Email',
                'error_code' => 1622016015,
            ],200);
        }

        /* Token erstellen */
        $token = $user->createToken('apptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response,201);
    }

    public function logout(Request $request) {

        /* Token in DB loeschen */
        auth()->user()->tokens()->delete();

        return ['message' => 'logged out'];
    }
}
