<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class projectcolumns extends Model
{
    use HasFactory;

    protected $fillable = [ //PostAnfrage
        'name',
        'projectId',
        'sort'
    ];

    /*  Datenbank Migration:
     *      $table->String('name');
            $table->integer('projectId');
            $table->integer('sort'); // 1 == user , 2 == moderator , 3 == admin....
     */
}
