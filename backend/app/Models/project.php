<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    use HasFactory;

    protected $fillable = [ //PostAnfrage
        'name',
        'adminid',
        'image', //Base64 Image
        'description'
    ];

    /*
        Datenbank Migration:
            $table->timestamps();
            $table->string('name');
            $table->integer('adminid');
            $table->longText('image');
            $table->string('desc');
     */


}
