import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        user: {
            created_at:'',
            email:'',
            email_verified_at: null,
            id: null,
            name: '',
            updated_at:'',
        },
        token: '',
    },
    getters: {
        user: state => {
            return state.user;
        },
        token: state => {
            return state.token;
        }
    },
    mutations: {
        user (state, data) {
            if(data.param) {
                state.user[data.param] = data.value;
            } else {
                state.user = data || {};
            }
        },
        token (state, data) {
            state.token = data;
        }
    }
})
