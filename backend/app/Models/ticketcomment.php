<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticketcomment extends Model
{
    use HasFactory;

    protected $fillable = [ //PostAnfrage
        'userId',
        'description',
        'ticketId'
    ];

    /* Datenbank Migration
     *  $table->integer('userId');
            $table->String('description');
            $table->integer('ticketId');
     *
     *
     */
}
