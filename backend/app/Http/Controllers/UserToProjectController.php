<?php

namespace App\Http\Controllers;

use App\Models\usertoproject;
use Illuminate\Http\Request;
use App\Models\project; //Auf das Model zeigen
use App\Models\User;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Psr7\copy_to_string;

class UserToProjectController extends Controller
{
    //TODO
    public function show($id) {
        return project::find($id);
    }

    //Beispiel mit User Auth.
    public function showUserProjects() {
        $user = auth()->user();
        $response = DB::select('SELECT * from usertoprojects where userId = :id', ['id'=>$user->id]);
        return response($response,200);
    }

    public function addUserToProject(Request $request) {

        $email = $request['email'];

        $userId = DB::select('SELECT id FROM users WHERE email = :email', ['email'=>$email]);

        if($userId){
            $response = DB::table('usertoprojects')->insert([
                'userId' => $userId[0]->id,
                'projectId' => $request['projectId'],
                'userType' => 0
            ]);
            return response($response,200);
        } else {
            return response(['message' => 'Kein User vorhanden'],404);
        }

    }

}
