<?php

namespace App\Http\Controllers;

use App\Models\projectcolumns;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class ProjectColumnsController extends Controller
{
    //
    public function showProjectColumns($id)
    {
        $user = auth()->user();

        //Prüfen ob User zugriff hat.
        $sql = DB::select("SELECT * from usertoprojects where userId = :uid AND projectId = :pid", ['uid'=> $user->id, 'pid'=>$id]);

        if($sql){
            $allColumns = DB::select('SELECT * from projectcolumns where projectId= :id', ['id' => $id]);

            if(!$allColumns){
                return response('', 204);
            }
            return response($allColumns, 200);
        }

        return response('', 204);
    }

    // Übergabe der Collumn ID
    public function update($id, Request $request) { //JSON {}
        $user = auth()->user();
        $pid = DB::select('SELECT projectId from `projectcolumns` where id = :id', ['id'=>$id]);
        $pid = $pid[0]->projectId;
        if($pid){
            $userType= DB::select('SELECT userType FROM `usertoprojects` where userId = :uid AND projectId = :pid', ['uid'=>$user->id, 'pid'=>$pid]);

            //UserType = 1 - Normaler User 2- moderator - 3 admin
            if($userType[0]->userType > 1) {
                $Columns = projectcolumns::findOrFail($id);
                $Columns->update($request->all());
                return $Columns;
            }
        }

        return response('', 204);
    } //name, projectId, sort

}
