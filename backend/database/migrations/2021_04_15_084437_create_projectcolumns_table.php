<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectcolumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projectcolumns', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->String('name');
            $table->integer('projectId');
            $table->integer('sort'); // 1 == user , 2 == moderator , 3 == admin....

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projectcolumns');
    }
}
