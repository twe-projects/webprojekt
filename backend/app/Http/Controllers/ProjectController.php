<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\project;;
use Illuminate\Support\Facades\DB;
use App\Models\User;

//Auf das Model zeigen


class ProjectController extends Controller
{

    //Daten aus der Datenbank anhand der ID holen
    public function show($id) {
        return project::find($id);
    }

     //Daten aus der Datenbank anhand der ID holen
     public function update($id, Request $request) { //JSON {}
        $project = project::findOrFail($id);
        $project->update($request->all());
        return $project;
    }

    public function getAllUser($id){


        $allProjectUsers = DB::select('SELECT p.projectId, p.userId, u.name from usertoprojects p LEFT JOIN users u on p.userId = u.id where p.projectId = :pid', ['pid' => $id]);

        return $allProjectUsers;

    }

    public function store(Request $request){ //name , adminid, image, description
        $user = auth()->user();

        $project = project::create($request->all());

        $response = DB::table('usertoprojects')->insert([
            'userId' => $user->id,
            'projectId' => $project->id,
            'userType' => 0
        ]);


        return $project;
    }

     //Daten aus der Datenbank anhand der ID holen
     public function delete($id) {
        $project = project::findOrFail($id);
        $project->delete();

        return response([
            'message' => 'successfully'
        ],204);
    }

    public function showAllUserProjects()
    {
        $user = auth()->user();
        $allProjects = DB::select('SELECT * from usertoprojects where userId = :id', ['id' => $user->id]);

        if(!$allProjects){
            return response('', 204);
        }
        $arr = [];

        foreach ($allProjects as $i => $value) {
            $arr[] = $value->projectId;
        };

        $sqlstring = "SELECT projects.id, projects.name AS project_name, projects.description, projects.image, users.name from projects LEFT JOIN users ON projects.adminid = users.id WHERE projects.id in (";
        $sqlstring = $sqlstring . join(",", $arr);
        $sqlstring = $sqlstring . ") ";

        $sql = DB::select($sqlstring);

        return response($sql, 200);
    }

    public function getQuantityProject()
    {
        $user = auth()->user();
        $allProjects = DB::select('SELECT COUNT(*) AS anzahl from usertoprojects where userId = :id', ['id' => $user->id]);

        if(!$allProjects){
            return response('', 204);
        } else {
            return response($allProjects, 200);
        }

    }

    public function getProjectAdmin($id){


        $admin = DB::select('SELECT adminid from projects p where p.id = :pid', ['pid' => $id]);

        return $admin;

    }

}
